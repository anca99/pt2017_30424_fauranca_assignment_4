package model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Observable;
import java.util.Observer;


public class Account implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7656222964184171065L;
	protected int ID;
	protected Float accountBalance;
	protected Date creationDate;
	protected ObservableType observ;

	public Account(){}
	public Account(Float accountBalance, Date dateCreated) {
		ID = 0;
		this.accountBalance =  accountBalance;
		this.creationDate = dateCreated;
		this.observ=new ObservableType();
	}
	
	
	///////////////////just for understanding
	public Account(Float accountBalance) {
		this.accountBalance =  accountBalance;
	}


	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public Float getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(Float accountBalance) {
		this.accountBalance = accountBalance;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String toString(){
		return "account "+this.getID()+" with sum= "+this.getAccountBalance();
	}
	
	public void deposit(float sum, Date date){
		System.out.println("m-am oprit sus la deposit");
	}
	
	public ObservableType getObservableType(){
		return observ;
	}
}
