package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

import GUI.PDF;

public class Person implements Serializable, Observer {
	private String ID;
	private String firstName;
	private String lastName;
	private int age;
	private String phone;
	private String address;
	private int noAccounts;

	public Person(String ID, String firstName, String lastName, int age, String phone, String address) {
		// assert this.isPersonConsideredAdult();
		this.ID = ID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.phone = phone;
		this.address = address;
		this.noAccounts=0;
	}

	public Person(ArrayList<Object>myFields){
		this.ID=(String)myFields.get(0);
		this.firstName=(String)myFields.get(1);
		this.lastName=(String)myFields.get(2);
		String forAge=myFields.get(3).toString();
		this.age=Integer.parseInt(forAge);
		this.phone=(String)myFields.get(4);
		this.address=(String)myFields.get(5);
		this.noAccounts=0;
	}
	
	public void incrementNoAccounts(){
		this.noAccounts++;
	}
	
	public void decrementNoAccounts(){
		this.noAccounts--;
	}
	
	public void setNoAccountsToZero(){
		this.noAccounts=0;
	}
	
	public int getNoAccounts(){
		return noAccounts;
	}

	public String getID() {
		return ID;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getAge(){
		return String.valueOf(age);
	}
	
	public String getPhone() {
		return phone;
	}

	public String getAddress() {
		return address;
	}

	public boolean isPersonConsideredAdult() {
		if (this.age >= 18)
			return true;
		return false;
	}

	public String toString() {
		return "id= "+this.ID + "  lastName=" + this.lastName + " firstName=" + this.firstName+ " no acc="+this.noAccounts;
	}
	
	public int hashCode() {
		 return ID.hashCode();
	}

	public boolean equals(Object o) {

		if (o == this)
			return true;
		if (!(o instanceof Person)) {
			return false;
		}

		Person p = (Person) o;
		Boolean result = p.ID.equals(ID);
		return result;
	}

	public void update(Observable o, Object arg) {
		System.out.println("Person "+this.firstName+" (" +ID+") has received a notification from one of his/her accounts");
		UUID help= UUID.randomUUID();
		String nameDoc=firstName+ID+help.toString();
		PDF.writeBill(nameDoc, arg.toString());	
	}
}
