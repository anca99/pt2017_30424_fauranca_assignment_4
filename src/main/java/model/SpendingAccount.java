package model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Observer;

public class SpendingAccount extends Account {


	public SpendingAccount() {
		super();
	};

	public SpendingAccount(Float sum, Date dateCreated) {
		super(sum, dateCreated);
		
	}

	public SpendingAccount(ArrayList<String> myFields) {
		super(Float.parseFloat((String) myFields.get(0)), Date.valueOf((String) myFields.get(1)));
		
	}

	public void withdraw(float sum, Date dat) {
		if (accountBalance < sum) {
			System.out.println("Impossible transaction: Not enough money in the spending account");
			return;
		}
		if (accountBalance - sum < 5) {
			System.out.println("Impossible transaction: The minimum accepted account balance is 5");
			return;
		}
		accountBalance -= sum;
		observ.setChanged();
		observ.notifyObservers("Account "+ID+": A withdraw of "+sum+" was performed on date "+dat.toString()+" current sum is "+accountBalance);
	}

	public void deposit(float sum, Date dat) {
		accountBalance += sum;
		observ.setChanged();
		observ.notifyObservers("Account "+ID+": A deposit of "+sum+" was performed on date "+dat.toString()+" current sum is "+accountBalance);
	}

}
