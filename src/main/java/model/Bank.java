package model;

import java.awt.Component;
import java.awt.List;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Bank implements BankProc, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6624593329121037344L;
	HashMap<Person, ArrayList<Account>> bankMagic;
	private int noPersonsRegistered;

	public Bank() {
		bankMagic = new HashMap<Person, ArrayList<Account>>();
		noPersonsRegistered = 0;
		assert isWellFormed();
	}

	
	public void addPerson(Person p) {
		assert p != null;
		assert bankMagic.containsKey(p) == false;
		assert isWellFormed();
		
		int sizePre = bankMagic.size();
		int noPersonRegisteredPre = noPersonsRegistered;

		ArrayList<Account> init = new ArrayList<Account>();
		bankMagic.put(p, init);
		noPersonsRegistered++;

		assert bankMagic.size() == sizePre + 1;
		assert noPersonsRegistered == noPersonRegisteredPre + 1;
		assert bankMagic.containsKey(p) == true;
		assert bankMagic.get(p).size() == 0;
		assert isWellFormed();

	}


	public void removePerson(Person p) {
		assert p != null;
		assert bankMagic.containsKey(p) == true;
		assert isWellFormed();
		
		
		int sizePre = bankMagic.size();
		int noPersonRegisteredPre = noPersonsRegistered;
		bankMagic.remove(p);
		noPersonsRegistered--;

		assert bankMagic.size() == sizePre - 1;
		assert noPersonsRegistered == noPersonRegisteredPre - 1;
		assert bankMagic.containsKey(p) == false;
		assert isWellFormed();

	}

	
	public void addAccountToPerson(Person p, Account a) {
		assert p != null;
		assert a != null;
		assert bankMagic.containsKey(p) == true;
		assert isWellFormed();
		int sizePre=bankMagic.get(p).size();
		

		ArrayList<Account> found = bankMagic.get(p);
		int indexLastPos = found.size();
		if (found.size() > 0) {
			Account lastElement = found.get(found.size() - 1);
			a.setID(lastElement.getID() + 1);
		}
		found.add(a);
		p.incrementNoAccounts();
		bankMagic.remove(p);
		bankMagic.put(p, found);

		assert bankMagic.get(p).contains(a) == true;
		assert bankMagic.get(p).size()==sizePre+1;
		assert isWellFormed();

	}

	public void editPerson(Person p){
		assert p != null;
		assert bankMagic.containsKey(p) == true;
		int sizePre=bankMagic.size();
		assert isWellFormed();
		
		ArrayList<Account> foundAccounts=bankMagic.get(p);
		int noAccounts=foundAccounts.size();
		bankMagic.remove(p);
		bankMagic.put(p, foundAccounts);
		
		System.out.println("I was here with "+p.getFirstName());
		displayContentHashMap();
		
		assert bankMagic.containsKey(p)==true;
		assert sizePre==bankMagic.size();
		assert noAccounts==bankMagic.get(p).size();
		assert isWellFormed();
	
	}

	public void removeAccountFromPerson(Person p, Account a) {
		assert p != null;
		assert a != null;
		assert bankMagic.containsKey(p) == true;
		assert bankMagic.get(p).contains(a) == true;
		assert isWellFormed();

		ArrayList<Account> found = bankMagic.get(p);
		int sizePre = found.size();
		for (int i = 0; i < found.size(); i++) {
			Account aux = found.get(i);
			if (aux.getID() == a.getID()) {
				found.remove(i);
				p.decrementNoAccounts();
				break;
			}
		}

		assert bankMagic.get(p).contains(a) == false;
		assert sizePre == bankMagic.get(p).size() + 1;
		assert isWellFormed();

	}

	
	public void removeAllAccountFromPerson(Person p) {
		assert p != null;
		assert bankMagic.containsKey(p) == true;
		assert isWellFormed();

		ArrayList<Account> found = bankMagic.get(p);
		ArrayList<Account> init = new ArrayList<Account>();
		bankMagic.put(p, init);
		p.setNoAccountsToZero();

		assert bankMagic.get(p).size() == 0;
		assert p.getNoAccounts() == 0;
		assert isWellFormed();
	}

	public void readAccount() {
		// TODO Auto-generated method stub

	}

	public void writeAccount() {
		// TODO Auto-generated method stub

	}

	public void displayContentHashMap() {
		System.out.println("\nThis is one print of the hash map");
		Set set = bankMagic.entrySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry mentry = (Map.Entry) iterator.next();
			System.out.print("key is: " + mentry.getKey() + " & Value is: ");
			System.out.println(mentry.getValue());
		}

	}

	
	public boolean isWellFormed() {
		Set set = bankMagic.entrySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry mentry = (Map.Entry) iterator.next();
			Person p = (Person) mentry.getKey();
			ArrayList<Account> listAccounts = (ArrayList<Account>) mentry.getValue();
			if (p == null || listAccounts == null) {
				return false;
			}	
		}
		return true;
	}

	
	public void serializeBank() {
		try {
			FileOutputStream fileOut = new FileOutputStream("bank.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(bankMagic);
			out.close();
			fileOut.close();
			System.out.printf("Serialized data is saved in bank.ser");
		} catch (IOException i) {
			i.printStackTrace();
		}
		
		assert isWellFormed();
	}

	public void deserializeBank() {
		try {
			FileInputStream fileIn = new FileInputStream("bank.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);

			this.bankMagic = (HashMap<Person, ArrayList<Account>>) in.readObject();
			in.close();
			fileIn.close();
		} catch (IOException ioi) {
			ioi.printStackTrace();
			return;
		} catch (ClassNotFoundException hopa) {
			hopa.printStackTrace();
			return;
		}
	}

	public HashMap<Person, ArrayList<Account>> getBankMagic() {
		return bankMagic;
	}


	public void setBankMagic(HashMap<Person, ArrayList<Account>> bankMagic) {
		this.bankMagic = bankMagic;
	}

	public Account findAccountFrom(Person p,String foundMe) {
		assert isWellFormed();
		assert p!=null;
		assert foundMe!=null;
		assert bankMagic.containsKey(p)==true;
		assert bankMagic.get(p).size()!=0;
		ArrayList<Account> myAccounts=bankMagic.get(p);
		for(Account a:myAccounts){
			String helper=Integer.toString(a.getID());
			System.out.println(helper+"="+foundMe+helper.equals(foundMe));
			if (foundMe.equals(helper)==true) {
				return a;
			}
		}
		
		assert isWellFormed();
		return null;
		
	}
	
	
}
