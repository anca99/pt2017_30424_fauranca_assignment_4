package model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Observer;
import java.util.concurrent.TimeUnit;

public class SavingAccount extends Account {
	private Date maturityDate;

	public SavingAccount() {
		super();
	};

	public SavingAccount(Float sum, Date dateCreated, Date maturityDate) {
		super(sum, dateCreated);
		this.maturityDate = maturityDate;

	}

	public SavingAccount(ArrayList<?> myFields) {
		super(Float.parseFloat((String) myFields.get(0)), Date.valueOf((String) myFields.get(1)));
		maturityDate = Date.valueOf((String) myFields.get(2));
	}

	/*
	 * formula for interest
	 * 
	 * sum* 5% * no_years=> gained interest
	 */
	/**
	 * Get a diff between two dates
	 * 
	 * @param date1
	 *            the oldest date
	 * @param date2
	 *            the newest date
	 * @param timeUnit
	 *            the unit in which you want the diff
	 * @return the diff value, in the provided unit
	 */
	public int getDateDiff() {
		long diffInMillies = maturityDate.getTime() - creationDate.getTime();
		long days = (int) TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
		return (int) days / 365;
	}

	public float computedInterest() {
		int years = getDateDiff();
		float interest = (float) (accountBalance * 0.05 * years);
		return interest;
	}

	public void withdraw(Date accessDate) {
		int gain = 0;
		if (accessDate.equals(maturityDate)) {
			System.out.println("The initial amount was " + this.accountBalance
					+ " and the access date is the same with the maturity date\n");
			System.out.println("GAIN!\n");
			accountBalance += computedInterest();
			System.out.println("The withdrawal is equal to " + accountBalance + "\n");
			gain = 1;
		} else {
			System.out.println("The initial amount was " + this.accountBalance
					+ " and the access date is different from the maturity date\n");
			System.out.println("PUNISHMENT OF 8% OF TOTAL SUM !\n");
			accountBalance -= (float) (accountBalance * 0.08);
			System.out.println("The withdrawal is equal to " + accountBalance + "\n");

		}
		observ.setChanged();
		if (gain == 1) {
			observ.notifyObservers("Account"+this.ID+": Congratulations, you gained the interest! A withdraw equal to " + accountBalance
					+ "was performed on the saving account and as effect the account was deleted");
		} else {
			observ.notifyObservers("Account"+this.ID+": Punishment of 8% of account balance! A withdraw equal to " + accountBalance
					+ "was performed on the saving account and as effect the account was deleted");
		}
	}

	public void addObserver(Observer obs) {
		observ.addObserver(obs);
	}

	public void deleteObserver(Observer obs) {
		observ.deleteObserver(obs);
	}
}
