package model;

public interface BankProc {

	/**
	 * Adds a new person in the hash map, initially its Account arrayList is
	 * initialized with an empty arrayList
	 * 
	 * @param p
	 *            Person - the person to be added in the hash map
	 * @precondition p!=null
	 * @precondition bankMagic.containsKey(p)==false;
	 * @post bankMagic.size()== bankMagic.size() @precondition +1;
	 * @post noPersonsRegistered= noPersonRegistered@precondition +1;
	 * @post bankMagic.get(p).size()==0; // size of the init array of accounts
	 *       is 0
	 */
	public void addPerson(Person p);

	/**
	 * Removes a person from the hash map
	 * 
	 * @param p
	 *            Person - person to be removed from the hash map
	 * @precondition p!=null
	 * @precondition bankMagic.containsKey(p)==true;
	 * @post bankMagic.size()== bankMagic.size() @precondition -1;
	 * @post noPersonsRegistered= noPersonRegistered@precondition -1;
	 * @post bankMagic.containsKey(p)==false;
	 * 
	 */
	public void removePerson(Person p);

	/**
	 * Adds an account to a person, its unique id is the increment of the last
	 * account in the list
	 * 
	 * @param p
	 *            Person -person to which the account is added
	 * @param a
	 *            The added account
	 * @precondition p!=null
	 * @precondition a!=null
	 * @precondition bankMagic.containsKey(p)==true;
	 * @post bankMagic.get(p).contains(a)==true; the account was added in the
	 *       list of accounts
	 * @post a.getID()==indexLastPos+1; the account has a correct id
	 * @post bankMagic.get(p).size()==sizePre+1;
	 */
	public void addAccountToPerson(Person p, Account a);

	/**
	 *Edit a person
	 * 
	 * @param p
	 *            Person -to be edited
	 * @precondition p!=null
	 * @precondition bankMagic.containsKey(p)==true;
	 * @post bankMagic.get(p).contains(a)==true;
	 * @post sizePre==bankMagic.size();
	 * @post noAccounts==bankMagic.get(p).size();
	 */
	public void editPerson(Person p);

	/**
	 * Remove an account from a person
	 * 
	 * @param p
	 *            Person -person to which the account is removed
	 * @param a
	 *            the account to be deleted
	 * @precondition p!=null
	 * @precondition a!=null
	 * @precondition bankMagic.containsKey(p)==true;
	 * @precondition bankMagic.get(p).contains(a)==true;
	 * @post bankMagic.get(p).contains(a)==false;
	 * @post sizePre==bankMagic.get(p).size();
	 */
	public void removeAccountFromPerson(Person p, Account a);

	/**
	 * Remove all accounts from a person
	 * 
	 * @param p
	 *            Person -person to which all accounts must be deleted
	 * @precondition p!=null
	 * @precondition bankMagic.containsKey(p)==true;
	 * @post bankMagic.get(p).size()==0;
	 * @post p.getNoAccounts()==0;
	 */
	public void removeAllAccountFromPerson(Person p);

	/**
	 * checks if the object is correct
	 * 
	 * @invariant isWellFormed()
	 * @return state of the object is true if it is well formed and false
	 *         otherwise
	 */
	public boolean isWellFormed();

	/**
	 * writes persons and their arrayLists of accounts in bank.ser file
	 */
	public void serializeBank();

	/**
	 * reads persons and their arrayLists of accounts from bank.ser file
	 */
	public void deserializeBank();

}
