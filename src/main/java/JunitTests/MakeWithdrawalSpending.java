package JunitTests;

import java.sql.Date;

import junit.framework.TestCase;
import model.Bank;
import model.Person;
import model.SpendingAccount;

public class MakeWithdrawalSpending extends TestCase {
	public void test(){
			Bank banky=new Bank();
			Person p=new Person("2960603014563", "Faur", "Anca Maria", 20, "0757391505", "Street Livezii no 49");
			banky.addPerson(p);
			
			String dat1="2006-03-02";
			String dat2="2017-03-06";
			SpendingAccount a=new SpendingAccount((float)2500, Date.valueOf(dat1));
			
			banky.addAccountToPerson(p, a);
			
			a.withdraw(200, Date.valueOf(dat2));
			
		}
}

