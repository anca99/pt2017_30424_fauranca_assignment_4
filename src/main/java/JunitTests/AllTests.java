package JunitTests;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


public class AllTests extends TestCase {
	public static Test suite() {
		TestSuite suite = new TestSuite(AllTests.class.getName());
		//$JUnit-BEGIN$
		suite.addTestSuite(AddPerson.class);
		suite.addTestSuite(RemovePerson.class);
		suite.addTestSuite(EditPerson.class);
		suite.addTestSuite(AddAccountSaving.class);
		suite.addTestSuite(AddAccountSpending.class);
		suite.addTestSuite(RemoveAccount.class);
		suite.addTestSuite(MakeDepositSpending.class);
		suite.addTestSuite(MakeWithdrawalSaving.class);
		suite.addTestSuite(MakeWithdrawalSpending.class);
		
		//$JUnit-END$
		return suite;
	}
}
