package FaurAnca30424H4.assig4;

import model.Account;
import model.Bank;
import model.Person;

public class App {
	public static void main(String[] args) {
		Bank b = new Bank();
		
		//b.deserializeBank();
		
		Person p1 = new Person("1", "Cosmin", "Dinu", 18, "lapte", "07464");
		Person p2 = new Person("2", "Eugen", "Sandache", 18, "lapte", "07464");
		Person p3 = new Person("3", "Dorin", "Teurean", 18, "lapte", "07464");
		Person p4 = new Person("4", "Luna", "Buna", 15, "laete", "0997464");
		
		Account a1 = new Account((float) 99.3);
		Account a2 = new Account((float) 88.9);
		Account a3 = new Account((float) 12.5);
		
		b.addPerson(p1);
		b.addPerson(p2);
		b.addPerson(p3);
		b.addPerson(p4);
		
		b.displayContentHashMap();
		
		b.addAccountToPerson(p1, a1);
		b.addAccountToPerson(p1, a2);
		b.addAccountToPerson(p2, a3);

		
		b.displayContentHashMap();
		
		
		b.removeAccountFromPerson(p1, a2);
		b.removeAllAccountFromPerson(p2);
		b.displayContentHashMap();
		
		b.removePerson(p4);
		b.displayContentHashMap();
		
		//b.serializeBank();
		
	}
}
