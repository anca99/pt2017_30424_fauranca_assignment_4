package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddAccount extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField creationDate;
	private JTextField accountBalance;
	private JTextField maturityDate;
	private JComboBox type;
	private MainApp mainy;
	
	
	   private void clearFields(){
	    	creationDate.setText("");
	    	accountBalance.setText("");
	    	maturityDate.setText("");
	    }
	   
	public AddAccount(MainApp main) {
		mainy=main;
		setTitle("Add Account\r\n");
		
		setBounds(100, 100, 471, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblAccountBalance = new JLabel("Account balance");
			lblAccountBalance.setFont(new Font("Segoe Script", Font.PLAIN, 14));
			lblAccountBalance.setBounds(100, 45, 152, 14);
			contentPanel.add(lblAccountBalance);
		}
		{
			JLabel lblCreationDate = new JLabel("Creation date");
			lblCreationDate.setFont(new Font("Segoe Script", Font.PLAIN, 13));
			lblCreationDate.setBounds(100, 85, 126, 14);
			contentPanel.add(lblCreationDate);
		}
		{
			JLabel lblType = new JLabel("Type");
			lblType.setFont(new Font("Segoe Print", Font.PLAIN, 14));
			lblType.setBounds(100, 126, 46, 20);
			contentPanel.add(lblType);
		}
		{
			JLabel lblIfTheChoosen = new JLabel("If the choosen type is saving account, \r\nplease write the maturity date of the account");
			lblIfTheChoosen.setFont(new Font("SimSun", Font.PLAIN, 11));
			lblIfTheChoosen.setBounds(10, 157, 433, 31);
			contentPanel.add(lblIfTheChoosen);
		}
		
		type = new JComboBox();
		type.setBounds(262, 127, 181, 20);
		contentPanel.add(type);
		type.addItem("Spending account");
		type.addItem("Saving account");
		
		creationDate = new JTextField();
		creationDate.setBounds(262, 82, 181, 20);
		contentPanel.add(creationDate);
		creationDate.setColumns(10);
		
		accountBalance = new JTextField();
		accountBalance.setBounds(262, 43, 181, 20);
		contentPanel.add(accountBalance);
		accountBalance.setColumns(10);
		
		maturityDate = new JTextField();
		maturityDate.setBounds(263, 197, 181, 20);
		contentPanel.add(maturityDate);
		maturityDate.setColumns(10);
		
		JLabel lblMaturityDate = new JLabel("Maturity date");
		lblMaturityDate.setFont(new Font("Segoe Print", Font.PLAIN, 14));
		lblMaturityDate.setBounds(100, 200, 126, 14);
		contentPanel.add(lblMaturityDate);
		{
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					dispose();
				}
			});
			cancelButton.setBounds(10, 227, 87, 23);
			contentPanel.add(cancelButton);
			cancelButton.setActionCommand("Cancel");
		}
		{
			JButton save = new JButton("Save");
			save.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					mainy.fieldsAddAccount[0]=accountBalance.getText();
					mainy.fieldsAddAccount[1]=creationDate.getText();
					mainy.fieldsAddAccount[2]= maturityDate.getText();
					mainy.fieldsAddAccount[3]=(String) type.getSelectedItem();
					mainy.addNewDataAcc();
					
					clearFields();
					setVisible(false);
					
				}
			});
			save.setBounds(350, 227, 93, 23);
			contentPanel.add(save);
			save.setActionCommand("OK");
			getRootPane().setDefaultButton(save);
		}
	}

}
