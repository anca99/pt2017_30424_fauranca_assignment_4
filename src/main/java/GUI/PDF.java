package GUI;

import java.io.FileOutputStream;


import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

public class PDF {
   
 public static void writeBill(String nameDoc, String content){
    	 Document document = new Document();	
    	 try{
   
    		 PdfWriter.getInstance(document, new FileOutputStream(nameDoc));
             document.open();
             Paragraph p1 = new Paragraph("ACCOUNT NOTIFICATION");
             Paragraph p2 = new Paragraph(content);
             document.add(p1);
             document.add(p2);
    	 }
         catch(Exception e){
             System.out.println(e);
         }
         document.close();
     }

}