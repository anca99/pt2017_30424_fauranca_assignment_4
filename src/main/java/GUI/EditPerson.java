package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.Person;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;

public class EditPerson extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField firstName;
	private JTextField lastName;
	private JTextField age;
	private JTextField phone;
	private JTextField address;
	private MainApp mainy;
	private Person personSelected;
		
	public void fillMyFields(){
		lastName.setText(personSelected.getLastName());
		firstName.setText(personSelected.getFirstName());
		age.setText(personSelected.getAge());
		phone.setText(personSelected.getPhone());
		address.setText(personSelected.getAddress());	
	}
	
	 
    private void clearFields(){
    	firstName.setText("");
    	lastName.setText("");
    	phone.setText("");
    	address.setText("");
    	age.setText("");
    }
    
	public EditPerson(MainApp main) {
		
		this.mainy=main;
		
		personSelected=new Person("dummy", "dummy","dummy",0, "dummy", "dummy");
		setBounds(100, 100, 310, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblFirstName = new JLabel("First Name");
			lblFirstName.setFont(new Font("Segoe Print", Font.PLAIN, 14));
			lblFirstName.setBounds(28, 48, 121, 14);
			contentPanel.add(lblFirstName);
		}
		{
			JLabel lblLastName = new JLabel("Last Name");
			lblLastName.setFont(new Font("Segoe Script", Font.PLAIN, 14));
			lblLastName.setBounds(27, 79, 106, 14);
			contentPanel.add(lblLastName);
		}
		{
			JLabel lblAge = new JLabel("Age");
			lblAge.setFont(new Font("Segoe Print", Font.PLAIN, 14));
			lblAge.setBounds(28, 104, 46, 16);
			contentPanel.add(lblAge);
		}
		{
			JLabel lblPhone = new JLabel("Phone");
			lblPhone.setFont(new Font("Segoe Script", Font.PLAIN, 14));
			lblPhone.setBounds(28, 131, 74, 14);
			contentPanel.add(lblPhone);
		}
		{
			JLabel lblAddress = new JLabel("Address");
			lblAddress.setFont(new Font("Segoe Print", Font.PLAIN, 14));
			lblAddress.setBounds(28, 156, 86, 14);
			contentPanel.add(lblAddress);
		}
		
		firstName = new JTextField();
		firstName.setBounds(159, 46, 125, 20);
		contentPanel.add(firstName);
		firstName.setColumns(10);
		{
			lastName = new JTextField();
			lastName.setBounds(159, 77, 125, 20);
			contentPanel.add(lastName);
			lastName.setColumns(10);
		}
		{
			age = new JTextField();
			age.setBounds(159, 103, 125, 20);
			contentPanel.add(age);
			age.setColumns(10);
		}
		{
			phone = new JTextField();
			phone.setBounds(159, 129, 125, 20);
			contentPanel.add(phone);
			phone.setColumns(10);
		}
		{
			address = new JTextField();
			address.setBounds(159, 154, 125, 20);
			contentPanel.add(address);
			address.setColumns(10);
		}
		
		
		{
			JButton cancelButton = new JButton("Cancel");
			cancelButton.setBounds(10, 227, 74, 23);
			contentPanel.add(cancelButton);
			cancelButton.setActionCommand("Cancel");
		}
		{
			JButton okButton = new JButton("SAVE");
			okButton.setBounds(215, 227, 69, 23);
			contentPanel.add(okButton);
			
			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					mainy.fieldsEditPerson[0]=firstName.getText();
					mainy.fieldsEditPerson[1]=lastName.getText();
					mainy.fieldsEditPerson[2]=age.getText();
					mainy.fieldsEditPerson[3]=phone.getText();
					mainy.fieldsEditPerson[4]=address.getText();
					mainy.editDataPers();
					clearFields();
					setVisible(false);
				}
			});
			okButton.setActionCommand("OK");
			getRootPane().setDefaultButton(okButton);
		}
	}
	
	
	public void setPersonSelected(Person persSel){
		personSelected=persSel;
	}
}
