package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;

public class MessageDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	JLabel message;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			MessageDialog dialog = new MessageDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setMessage(String mess){
		message.setText(mess);
	}
	
	public MessageDialog() {
		setBounds(100, 100, 382, 224);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			message = new JLabel("The message");
			message.setHorizontalAlignment(SwingConstants.CENTER);
			message.setBounds(10, 29, 346, 32);
			contentPanel.add(message);
		}
		
		JButton okay = new JButton("OK");
		okay.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				setVisible(false);
			}
		});
		okay.setBounds(143, 106, 89, 23);
		contentPanel.add(okay);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
		}
	}
}
