package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Withdraw extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField sum;
	private JTextField date;
	private MainApp mainy;

	private void clearFields() {
		sum.setText("");
		date.setText("");
	}

	public Withdraw(MainApp main) {
		mainy = main;

		setTitle("Make a withdrawal");
		setBounds(100, 100, 418, 191);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JButton okButton = new JButton("SAVE");
			okButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					mainy.fieldsWithdraw[0] = sum.getText();
					mainy.fieldsWithdraw[1] = date.getText();
					mainy.makeWithdraw();
					clearFields();
					setVisible(false);
				}
			});
			okButton.setBounds(320, 120, 72, 23);
			contentPanel.add(okButton);
			okButton.setActionCommand("SAVE\r\n");
			getRootPane().setDefaultButton(okButton);
		}
		{
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
				}
			});
			cancelButton.setBounds(10, 120, 65, 23);
			contentPanel.add(cancelButton);
			cancelButton.setActionCommand("Cancel");
		}
		{
			JLabel lblSum = new JLabel("Sum");
			lblSum.setFont(new Font("Segoe Script", Font.BOLD, 14));
			lblSum.setBounds(130, 40, 46, 14);
			contentPanel.add(lblSum);
		}
		{
			sum = new JTextField();
			sum.setBounds(176, 37, 216, 20);
			contentPanel.add(sum);
			sum.setColumns(10);
		}
		{
			date = new JTextField();
			date.setBounds(176, 68, 216, 20);
			contentPanel.add(date);
			date.setColumns(10);
		}
		{
			JLabel lblNewLabel = new JLabel("Date\r\n");
			lblNewLabel.setFont(new Font("Segoe Print", Font.BOLD, 14));
			lblNewLabel.setBounds(130, 70, 59, 14);
			contentPanel.add(lblNewLabel);
		}
		
		JLabel lblNewLabel_1 = new JLabel("If withdrawal from saving account, the sum is not required");
		lblNewLabel_1.setBounds(10, 11, 363, 14);
		contentPanel.add(lblNewLabel_1);
	}
}
