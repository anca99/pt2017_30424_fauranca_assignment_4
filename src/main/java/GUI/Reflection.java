package GUI;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Reflection {

	public static void retrieveProperties(Object object) {

		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true); // set modifier to public
			Object value;
			try {
				value = field.get(object);
				System.out.println(field.getName() + "=" + value);

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * Retrieves the field values of a given object through reflection
	 * techniques
	 * 
	 * @param object
	 *            object whose values must be retried
	 * @return row List of fields values for the given object
	 */
	public static ArrayList<Object> retrieveRowsForTable(Object object) {
		List row = new ArrayList<Object>();

		if (object.getClass().getSuperclass() != null) {
			for (Field field : object.getClass().getSuperclass().getDeclaredFields()) {
				field.setAccessible(true);
				if (field.getName().equals("serialVersionUID")==false &&field.getName().equals("observ")==false) {
					try {
						row.add(field.get(object));

					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}

				}
			}
		}
		for (Field field : object.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			try {
				row.add(field.get(object));

			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return (ArrayList<Object>) row;
	}
}
