package GUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JTextArea;
import javax.swing.JOptionPane;

public class MainApp extends JFrame {

	private Bank banky;

	private JPanel contentPane;
	private JTable savAccountJ;
	private JTable spendAccountJ;
	private JTable persoaneJ;
	private final DefaultTableModel model;
	private final DefaultTableModel model1;
	private final DefaultTableModel model2;

	private JScrollPane scrollPane;
	private JScrollPane scrollPane2;
	private JScrollPane scrollPane3;

	private Person personSelected;
	private Account accountSelected;

	public AddPerson diagAddPers;
	public EditPerson diagEditPers;
	public AddAccount diagAddAcc;
	public Deposit diagDeposit;
	public Withdraw diagWithdraw;
	public MessageDialog diagMessage;


	public static String[] fieldsAddPerson;
	public static String[] fieldsEditPerson;
	public static String[] fieldsAddAccount;
	public static String[] fieldsDeposit;
	public static String[] fieldsWithdraw;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainApp frame = new MainApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void addNewDataPers() {
		Person p = new Person(fieldsAddPerson[0], fieldsAddPerson[1], fieldsAddPerson[2],
				Integer.parseInt(fieldsAddPerson[3]), fieldsAddPerson[4], fieldsAddPerson[5]);
		banky.addPerson(p);
		try {
			postDataPersons("Person");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void editDataPers() {
		Person p = new Person(personSelected.getID(), fieldsEditPerson[0], fieldsEditPerson[1],
				Integer.parseInt(fieldsEditPerson[2]), fieldsEditPerson[3], fieldsEditPerson[4]);
		System.out.println("This is the ID  selected for person edit:" + personSelected.getID());
		banky.editPerson(p);
		try {
			postDataPersons("Person");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		personSelected=null;
		clean(model);
		clean(model1);
	}

	public void addNewDataAcc() {
		
		
		if (fieldsAddAccount[3].equals("Spending account")) {
			Date dateCreated = Date.valueOf(fieldsAddAccount[1]);
			SpendingAccount a = new SpendingAccount(Float.parseFloat(fieldsAddAccount[0]), dateCreated);
			
			banky.addAccountToPerson(personSelected, a);
			System.out.println("spending account added" + a.toString());
			try {
				postDataAccounts();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  } else if (fieldsAddAccount[3].equals("Saving account")) {
			Date dateCreated = Date.valueOf(fieldsAddAccount[1]);
			Date dateMaturity = Date.valueOf(fieldsAddAccount[2]);
			SavingAccount a = new SavingAccount(Float.parseFloat(fieldsAddAccount[0]), dateCreated, dateMaturity);
			banky.addAccountToPerson(personSelected, a);
			System.out.println("saving account added" + a.toString());
			try {
				postDataAccounts();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			personSelected=null;
			clean(model);
			clean(model1);
		}
	}

	private void clean(DefaultTableModel model2) {
		int i;
		int countMe = model2.getRowCount();
		for (i = countMe - 1; i >= 0; i--) {
			model2.removeRow(i);
		}
	}

	public void postDataPersons(String className)
			throws ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// initialize jtable
		clean(model2);

		int i;
		Set set = banky.getBankMagic().entrySet();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			Map.Entry mentry = (Map.Entry) iterator.next();
			Person p = (Person) mentry.getKey();
			Vector subVector = new Vector<Object>(Reflection.retrieveRowsForTable(p));
			model2.addRow(subVector);
		}
	}

	public void postDataAccounts() throws ClassNotFoundException, NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// initialize jtables
		clean(model);
		clean(model1);

		int i;
		ArrayList<Account> accountsSelected = banky.getBankMagic().get(personSelected);
		for (Account a : accountsSelected) {
			a.getObservableType().addObserver(personSelected);
			Vector subvector = new Vector<Object>(Reflection.retrieveRowsForTable(a));
			SpendingAccount dummy = new SpendingAccount();
			if (a.getClass().equals(dummy.getClass())) {
				model1.addRow(subvector);
			} else {
				model.addRow(subvector);
			}
		}
	}

	public void makeDeposit(){

		int sum=Integer.parseInt(fieldsDeposit[0]);
		Date dat=Date.valueOf(fieldsDeposit[1]);
		try {
		    SpendingAccount acc = SpendingAccount.class.cast(accountSelected);
		    acc.deposit(sum, dat);
		    // No exception: obj is of type C or IT MIGHT BE NULL!
		} catch (ClassCastException e) {
			e.printStackTrace();
		}
		try {
			postDataAccounts();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		personSelected=null;
		accountSelected=null;
		clean(model);
		clean(model1);
	}
	
	public void makeWithdraw(){

		Date dat=Date.valueOf(fieldsWithdraw[1]);
		try {
		    SpendingAccount acc = SpendingAccount.class.cast(accountSelected);
		    int sum=Integer.parseInt(fieldsWithdraw[0]);
		    acc.withdraw(sum, dat);
		
		} catch (ClassCastException e) {
		
		}
		try {
		    SavingAccount acc =  SavingAccount.class.cast(accountSelected);
		    acc.withdraw(dat);
		    banky.removeAccountFromPerson(personSelected, accountSelected);
		   
		} catch (ClassCastException e) {
			
		}
		try {
			postDataAccounts();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		accountSelected=null;
		personSelected=null;
		clean(model);
		clean(model1);
	}
	
	
	public MainApp() {

		banky = new Bank();
		banky.deserializeBank();

		fieldsAddPerson = new String[6];
		fieldsEditPerson = new String[5];
		fieldsAddAccount = new String[4];
		fieldsDeposit = new String[2];
		fieldsWithdraw = new String[2];

		diagAddPers = new AddPerson(this);
		diagEditPers = new EditPerson(this);
		diagAddAcc = new AddAccount(this);
		diagDeposit = new Deposit(this);
		diagWithdraw = new Withdraw(this);
		diagMessage= new MessageDialog();
		
		
		setTitle("Bank Management");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 481, 392);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Spending Account");
		lblNewLabel.setFont(new Font("Segoe Script", Font.BOLD | Font.ITALIC, 16));
		lblNewLabel.setBounds(10, 142, 267, 26);
		contentPane.add(lblNewLabel);

		JLabel lblSavingAccounts = new JLabel("Saving Accounts");
		lblSavingAccounts.setFont(new Font("Segoe Script", Font.BOLD | Font.ITALIC, 16));
		lblSavingAccounts.setBounds(10, 246, 267, 26);
		contentPane.add(lblSavingAccounts);

		Object[] col = { "ID", "Account Balance", "Creation Date", "Maturity Date" };
		model = new DefaultTableModel(col, 0);
		savAccountJ = new JTable(model);
		savAccountJ.setBounds(10, 272, 267, 70);
		contentPane.add(savAccountJ);

		Object[] col1 = { "ID", "Account Balance", "Creation Date" };
		model1 = new DefaultTableModel(col1, 0);
		spendAccountJ = new JTable(model1);
		spendAccountJ.setBounds(10, 165, 267, 70);
		contentPane.add(spendAccountJ);

		Object[] col2 = { "ID", "First Name", "Last Name", "Age", "Phone", "Address" };
		model2 = new DefaultTableModel(col2, 0);
		persoaneJ = new JTable(model2);
		persoaneJ.setBounds(10, 35, 267, 96);
		contentPane.add(persoaneJ);

		try {
			postDataPersons("Person");
		} catch (Exception e) {
			e.printStackTrace();
		}

		persoaneJ.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int myRow = persoaneJ.getSelectedRow();
				int i;
				ArrayList<Object> forPerson = new ArrayList<Object>();

				for (i = 0; i < persoaneJ.getColumnCount(); i++) {
					forPerson.add(persoaneJ.getValueAt(myRow, i));
				}

				personSelected = new Person(forPerson);
				try {
					postDataAccounts();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		spendAccountJ.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int myRow = spendAccountJ.getSelectedRow();
				Object foundMe = spendAccountJ.getValueAt(myRow, 0);
				String helpy = foundMe.toString();
				accountSelected = banky.findAccountFrom(personSelected, helpy);
			}
		});

		savAccountJ.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int myRow = savAccountJ.getSelectedRow();
				Object foundMe = savAccountJ.getValueAt(myRow, 0);
				String helpy = foundMe.toString();
				accountSelected = banky.findAccountFrom(personSelected, helpy);
				
			}
		});
		
		JLabel lblPersons = new JLabel("Persons");
		lblPersons.setFont(new Font("Segoe Script", Font.BOLD | Font.ITALIC, 16));
		lblPersons.setBounds(10, 11, 137, 26);
		contentPane.add(lblPersons);

		JButton btnAddPerson = new JButton("Add person");
		btnAddPerson.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				diagAddPers.setVisible(true);
			}
		});

		btnAddPerson.setFont(new Font("Segoe Script", Font.BOLD, 14));
		btnAddPerson.setBounds(287, 31, 168, 23);
		contentPane.add(btnAddPerson);

		JButton btnRemovePerson = new JButton("Remove person");
		btnRemovePerson.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (personSelected==null){
					diagMessage.setMessage("Please select a person");
					diagMessage.setVisible(true);
				}else{
				banky.removePerson(personSelected);
				try {
					clean(model2);
					postDataPersons("Person");
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			}
		});
		btnRemovePerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnRemovePerson.setFont(new Font("Segoe Script", Font.BOLD, 14));
		btnRemovePerson.setBounds(287, 65, 168, 23);
		contentPane.add(btnRemovePerson);

		JButton btnEditPerson = new JButton("Edit person");
		btnEditPerson.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (personSelected==null){
					diagMessage.setMessage("Please select a person");
					diagMessage.setVisible(true);
				}else{
				diagEditPers.setVisible(true);
				diagEditPers.setPersonSelected(personSelected);
				diagEditPers.fillMyFields();
				}
				persoaneJ.getSelectionModel().clearSelection();
			}
		});
		btnEditPerson.setFont(new Font("Segoe Script", Font.BOLD, 14));
		btnEditPerson.setBounds(287, 99, 168, 23);
		contentPane.add(btnEditPerson);

		JButton btnAddAccount = new JButton("Add account");
		btnAddAccount.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (personSelected==null){
					diagMessage.setMessage("Please select a person");
					diagMessage.setVisible(true);
				}else{
				diagAddAcc.setVisible(true);
				savAccountJ.getSelectionModel().clearSelection();
				spendAccountJ.getSelectionModel().clearSelection();
				persoaneJ.getSelectionModel().clearSelection();
			}
			}
		});
		btnAddAccount.setFont(new Font("Segoe Script", Font.BOLD, 14));
		btnAddAccount.setBounds(287, 161, 168, 23);
		contentPane.add(btnAddAccount);

		JButton btnRemoveAccount = new JButton("Remove account");
		btnRemoveAccount.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if (personSelected==null){
					diagMessage.setMessage("Please select a person");
					diagMessage.setVisible(true);
				}else if (accountSelected==null){
					diagMessage.setMessage("Please select an account");
					diagMessage.setVisible(true);
				}else{
				banky.removeAccountFromPerson(personSelected, accountSelected);
				try {
					postDataAccounts();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			}
		});
		btnRemoveAccount.setFont(new Font("Segoe Script", Font.BOLD, 14));
		btnRemoveAccount.setBounds(287, 195, 168, 23);
		contentPane.add(btnRemoveAccount);

		JButton btnWithdraw = new JButton("Withdraw");
		btnWithdraw.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (accountSelected==null){
					diagMessage.setMessage("Please select an account");
					diagMessage.setVisible(true);
				}else{
				diagWithdraw.setVisible(true);
				savAccountJ.getSelectionModel().clearSelection();
				spendAccountJ.getSelectionModel().clearSelection();
				persoaneJ.getSelectionModel().clearSelection();
			}
			}
		});
		btnWithdraw.setFont(new Font("Segoe Script", Font.BOLD, 14));
		btnWithdraw.setBounds(287, 268, 168, 23);
		contentPane.add(btnWithdraw);

		JButton btnDeposit = new JButton("Deposit");
		btnDeposit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			SpendingAccount dummy=new SpendingAccount();
			if (accountSelected==null){
				 diagMessage.setMessage("Please select a spending account");
				 diagMessage.setVisible(true);
			}else{
				
				if (accountSelected.getClass().equals(dummy.getClass())){
					diagDeposit.setVisible(true);
					savAccountJ.getSelectionModel().clearSelection();
					spendAccountJ.getSelectionModel().clearSelection();
					persoaneJ.getSelectionModel().clearSelection();
				}
			}
			}
		});
		
		btnDeposit.setFont(new Font("Segoe Script", Font.BOLD, 14));
		btnDeposit.setBounds(287, 229, 168, 23);
		contentPane.add(btnDeposit);

		scrollPane = new JScrollPane(persoaneJ);
		getContentPane().add(scrollPane);
		scrollPane.setVisible(true);
		scrollPane.setBounds(10, 35, 267, 96);
		JPanel buttonPanel = new JPanel();
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);

		scrollPane2 = new JScrollPane(savAccountJ);
		getContentPane().add(scrollPane2);
		scrollPane2.setVisible(true);
		scrollPane2.setBounds(10, 272, 267, 70);
		JPanel buttonPanel2 = new JPanel();
		getContentPane().add(buttonPanel2, BorderLayout.SOUTH);

		scrollPane3 = new JScrollPane(spendAccountJ);
		getContentPane().add(scrollPane3);
		scrollPane3.setVisible(true);
		scrollPane3.setBounds(10, 165, 267, 70);
		JPanel buttonPanel3 = new JPanel();
		getContentPane().add(buttonPanel3, BorderLayout.SOUTH);

		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				banky.serializeBank();
				System.exit(0);
			}

		});
	}

}
