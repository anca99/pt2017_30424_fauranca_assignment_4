package GUI;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.InvocationTargetException;

public class AddPerson extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField id;
	private JTextField firstName;
	private JTextField lastName;
	private JTextField age;
	private JTextField phone;
	private JTextField address;
    private MainApp mainy;
	

	/**
	 * Create the dialog.
	 */
    
    private void clearFields(){
    	id.setText("");
    	firstName.setText("");
    	lastName.setText("");
    	phone.setText("");
    	address.setText("");
    	age.setText("");
    }
	public AddPerson(MainApp main) {
        mainy=main;
		setTitle("Add Person");
		setBounds(100, 100, 334, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblId = new JLabel("ID");
			lblId.setFont(new Font("Segoe Script", Font.PLAIN, 14));
			lblId.setBounds(28, 40, 46, 14);
			contentPanel.add(lblId);
		}
		{
			JLabel lblFirstName = new JLabel("First Name");
			lblFirstName.setFont(new Font("Segoe Print", Font.PLAIN, 14));
			lblFirstName.setBounds(28, 65, 121, 14);
			contentPanel.add(lblFirstName);
		}
		{
			JLabel lblLastName = new JLabel("Last Name");
			lblLastName.setFont(new Font("Segoe Script", Font.PLAIN, 14));
			lblLastName.setBounds(28, 90, 106, 14);
			contentPanel.add(lblLastName);
		}
		{
			JLabel lblAge = new JLabel("Age");
			lblAge.setFont(new Font("Segoe Print", Font.PLAIN, 14));
			lblAge.setBounds(28, 117, 46, 16);
			contentPanel.add(lblAge);
		}
		{
			JLabel lblPhone = new JLabel("Phone");
			lblPhone.setFont(new Font("Segoe Script", Font.PLAIN, 14));
			lblPhone.setBounds(28, 144, 74, 14);
			contentPanel.add(lblPhone);
		}
		{
			JLabel lblAddress = new JLabel("Address");
			lblAddress.setFont(new Font("Segoe Print", Font.PLAIN, 14));
			lblAddress.setBounds(28, 169, 86, 14);
			contentPanel.add(lblAddress);
		}
		
		id = new JTextField();
		id.setBounds(159, 38, 125, 20);
		contentPanel.add(id);
		id.setColumns(10);
		
		firstName = new JTextField();
		firstName.setBounds(159, 63, 125, 20);
		contentPanel.add(firstName);
		firstName.setColumns(10);
		{
			lastName = new JTextField();
			lastName.setBounds(159, 87, 125, 20);
			contentPanel.add(lastName);
			lastName.setColumns(10);
		}
		{
			age = new JTextField();
			age.setBounds(159, 114, 125, 20);
			contentPanel.add(age);
			age.setColumns(10);
		}
		{
			phone = new JTextField();
			phone.setBounds(159, 141, 125, 20);
			contentPanel.add(phone);
			phone.setColumns(10);
		}
		{
			address = new JTextField();
			address.setBounds(159, 166, 125, 20);
			contentPanel.add(address);
			address.setColumns(10);
		}
		
		JPanel panel = new JPanel();
		panel.setBounds(28, 217, 268, 33);
		contentPanel.add(panel);
		panel.setLayout(new FlowLayout(FlowLayout.RIGHT));
		{
			JButton cancelButton = new JButton("Cancel");
			panel.add(cancelButton);
			cancelButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					clearFields();
					setVisible(false); 
				}
			});
			cancelButton.setActionCommand("Cancel");
		}
		{
			JButton okButton = new JButton("SAVE");
			panel.add(okButton);
			okButton.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					
					mainy.fieldsAddPerson[0]=id.getText();
					mainy.fieldsAddPerson[1]=firstName.getText();
					mainy.fieldsAddPerson[2]=lastName.getText();
					mainy.fieldsAddPerson[3]=age.getText();
					mainy.fieldsAddPerson[4]=phone.getText();
					mainy.fieldsAddPerson[5]=address.getText();
					mainy.addNewDataPers();
					clearFields();
					setVisible(false);
					
				}
			});
			okButton.setActionCommand("OK");
			getRootPane().setDefaultButton(okButton);
		}
	}
}
